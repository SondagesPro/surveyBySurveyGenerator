<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2020 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.5.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class surveyBySurveyGenerator extends PluginBase
{
    protected $storage = 'DbStorage';
    static protected $description = 'Generate a survey with another survey using a base survey';
    static protected $name = 'surveyBySurveyGenerator';

    /**
    * Add function to be used in cron event
    * @see parent::init()
    */
    public function init()
    {
        /* Add the link in menu */
        $this->subscribe('beforeToolsMenuRender');
        $this->subscribe('afterSurveyComplete');
        $this->subscribe('beforeSurveyPage');
    }

    /**
     * If survey is a generator, only accept Admin user
     */
    public function beforeSurveyPage() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $iSurveyId = $this->getEvent()->get('surveyId');
        $baseSurveyId = $this->get('surveyBaseId', 'Survey', $iSurveyId);
        if(is_array($baseSurveyId)) {
            $baseSurveyId = end($baseSurveyId);
        }
        if(!$baseSurveyId) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        $oBaseSurvey = Survey::model()->findByPk($baseSurveyId);
        if($oBaseSurvey) {
            $oEvent = new PluginEvent('beforeSurveyGenerator');
            $oEvent->set('generatorId', $iSurveyId);
            $oEvent->set('sourceId', $baseSurveyId);
            App()->getPluginManager()->dispatchEvent($oEvent);
            $messageToShown = $oEvent->get('surveyBySurveyGeneratorMessage');
            if(!empty($messageToShown)) {
                $messageClass = $oEvent->get('surveyBySurveyGeneratorAlertType','warning');
                \renderMessage\messageHelper::renderAlert($messageToShown,$messageClass);
                //~ $messageToShown = '<div class="'.$messageClass.'">'.$messageToShown.'</div>';
                Yii::app()->end();
            }
        }
    }
    /** Menu and settings part */
    /**
     * see beforeToolsMenuRender event
     *
     * @return void
     */

    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if(Permission::model()->hasGlobalPermission('superadmin')) {
            $event = $this->getEvent();
            $surveyId = $event->get('surveyId');

            $href = Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            );
            $aMenuItem= array(
                'label' => $this->_translate('Survey generation'),
                'iconClass' => 'fa fa-cogs',
                'href' => $href
            );
            if (class_exists("\ls\menu\MenuItem")) {
                $menuItem = new \ls\menu\MenuItem($aMenuItem);
            } else {
                $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
            }
            $event->append('menuItems', array($menuItem));
        }
    }
    /**
     * Main function
     * @param int $surveyId Survey id
     *
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey=Survey::model()->findByPk($surveyId);
        if(!$oSurvey) {
            throw new CHttpException(404,$this->translate("This survey does not seem to exist."));
        }
        if(!Permission::model()->hasSurveyPermission($surveyId,'surveysettings','update')){
            throw new CHttpException(401);
        }
        if(!Permission::model()->hasGlobalPermission('superadmin')){
            throw new CHttpException(401);
        }
        if(App()->getRequest()->getPost('save'.get_class($this))) {
            $this->_saveSettings($surveyId);
        }
        $aSettings=array();
        LimeExpressionManager::SetDirtyFlag();
        LimeExpressionManager::SetSurveyId($surveyId);
        LimeExpressionManager::SetEMLanguage($oSurvey->language);
        LimeExpressionManager::StartSurvey($surveyId, 'survey', array('hyperlinkSyntaxHighlighting'=>true),true);
        LimeExpressionManager::NavigateForwards();
        $aOSurveys = Survey::model()->with('languagesettings')->findAll();
        // Construct the array
        $aSurveysSelect = array();
        foreach($aOSurveys as $oSurvey) {
            if(isset($oSurvey->languagesettings[$oSurvey->language])) {
                $aSurveysSelect[$oSurvey->sid] = date("Ymd",strtotime($oSurvey->datecreated))." ".$oSurvey->localizedTitle;
            } else {
                tracevar($oSurvey->sid." didnt have languagesettings for ".$oSurvey->language);
            }
        }
        $currentSurveyBaseId = $this->get('surveyBaseId', 'Survey', $surveyId,"");
        if(is_array($currentSurveyBaseId)) {
            $currentSurveyBaseId = end($currentSurveyBaseId);
        }
        $aSettings[$this->_translate('Survey selection')]=array(
            'surveyBaseId' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty'=>$this->_translate("None"),
                ),
                'options'=>$aSurveysSelect,
                'label'=>$this->_translate("Survey to be used for generation."),
                'current'=>$currentSurveyBaseId,
            ),
            'surveyBaseIdUpdate'=> array(
                'type'=>'link',
                'text'=> '<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),
                'label'=>'',
                'link' => '#',
                'htmlOptions'=> array(
                    'data-click-name'=>'save'.get_class($this),
                    'data-click-value'=>'save'
                ),
            ),
        );


        $aSettings[$this->_translate('Survey text element')]=array_merge($this->_getSurveyText($surveyId),$this->_getSurveyString($surveyId));
        $aSurveySettings = array();
        if($currentSurveyBaseId) {
            $aSurveySettings = $this->_getSurveySettings($surveyId,true);
        }
        $aData['pluginClass']=get_class($this);
        $aData['surveyId']=$surveyId;
        $aData['title']=$this->_translate("Survey generation");
        $aData['aSettings']=$aSettings;
        $aData['aSurveySettings'] = $aSurveySettings;
        $aData['assetUrl']=Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/');
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /**
     * Save settings using post value
     * @return void
     */
    private function _saveSettings($surveyId) {
        $currentSurvey = $this->get('surveyBaseId', 'Survey', $surveyId);
        if(is_array($currentSurvey)) {
            $currentSurvey = end($currentSurvey);
        }
        $newSurvey = App()->getRequest()->getPost('surveyBaseId');
        $disableRedirect = false;
        if($currentSurvey != $newSurvey) {
            $this->set('surveyBaseId', App()->getRequest()->getPost('surveyBaseId'), 'Survey', $surveyId);
            return; // Never update other part if survey id is updated
        }
        PluginSetting::model()->deleteAll("plugin_id = :plugin_id and model_id = :model_id", array(':plugin_id'=>$this->id,':model_id'=>$surveyId));
        $this->set('surveyBaseId', App()->getRequest()->getPost('surveyBaseId'), 'Survey', $surveyId);
        $groups = App()->getRequest()->getPost('group');
        if(is_array($groups) && !empty($groups)) {
            array_walk_recursive($groups,'trim');
            $this->set('groups',json_encode($groups), 'Survey', $surveyId);
        }
        $questions = App()->getRequest()->getPost('question');
        if(is_array($questions) && !empty($questions)) {
            array_walk_recursive($questions,'trim');
            $this->set('questions',json_encode($questions), 'Survey', $surveyId);
        }
        $subquestions = App()->getRequest()->getPost('subquestion');
        if(is_array($subquestions) && !empty($subquestions)) {
            array_walk_recursive($subquestions,'trim');
            $this->set('subquestions',json_encode($subquestions), 'Survey', $surveyId);
        }
        $answers = App()->getRequest()->getPost('answer');
        if(is_array($answers) && !empty($answers)) {
            array_walk_recursive($answers,'trim');
            $this->set('answers',json_encode($answers), 'Survey', $surveyId);
        }
        foreach($this->_getTextLabels($surveyId) as $text=>$label) {
            $this->set($text,App()->getRequest()->getPost($text), 'Survey', $surveyId);
        }
        foreach($this->_getStringLabels($surveyId) as $text=>$label) {
            $this->set($text,App()->getRequest()->getPost($text), 'Survey', $surveyId);
        }
        foreach($this->_getSettingsStringLabels($surveyId) as $text=>$label) {
            $this->set($text,App()->getRequest()->getPost($text), 'Survey', $surveyId);
        }
        if(App()->getRequest()->getPost('save'.get_class($this)=='redirect')) {
            Yii::app()->getController()->redirect(Yii::app()->getController()->createUrl('admin/survey',array('sa'=>'view','surveyid'=>$surveyId)));
        }
    }

    /**
     * Get the current settings for a survey
     * @param int surveyId
     * @param be sure to update/reset current value (EM session)
     * @return array[]
     **/
    private function _getSurveySettings($iSurveyId,$reset = true) {
        $baseSurveyId = $this->get('surveyBaseId', 'Survey', $iSurveyId);
        if(is_array($baseSurveyId)) {
            $baseSurveyId = end($baseSurveyId);
        }
        $oSurvey = Survey::model()->findByPk($baseSurveyId);
        Yii::import('application.helpers.viewHelper');

        $aSurveySettings=array();
        if(!$oSurvey) {
            return $aSurveySettings;
        }
        if($reset) {
            //~ LimeExpressionManager::SetDirtyFlag();
            //~ LimeExpressionManager::SetPreviewMode('logic');
        }
        $oGroups= QuestionGroup::model()->findAll(array(
            'condition'=>"sid=:sid and language=:language",
            'order'=>'group_order ASC',
            'params'=>array(":sid"=>$oSurvey->sid,":language"=>$oSurvey->language),
        ));
        $currentGroups = $this->get('groups', 'Survey', $iSurveyId);
        if(is_array($currentGroups)) {
            $currentGroups = end($currentGroups);
        }
        if($currentGroups) {
            $currentGroups = json_decode($currentGroups,true);
        }
        $currentQuestions = $this->get('questions', 'Survey', $iSurveyId);
        if(is_array($currentQuestions)) {
            $currentQuestions = end($currentQuestions);
        }
        if($currentQuestions) {
            $currentQuestions = json_decode($currentQuestions,true);
        }
        $currentSubQuestions = $this->get('subquestions', 'Survey', $iSurveyId);
        if(is_array($currentSubQuestions)) {
            $currentSubQuestions = end($currentSubQuestions);
        }
        if($currentSubQuestions) {
            $currentSubQuestions = json_decode($currentSubQuestions,true);
        }
        $currentAnswers = $this->get('answers', 'Survey', $iSurveyId);
        if(is_array($currentAnswers)) {
            $currentAnswers = end($currentAnswers);
        }
        if($currentAnswers) {
            $currentAnswers = json_decode($currentAnswers,true);
        }
        foreach($oGroups as $oGroup) {
            $groupName=viewHelper::flatEllipsizeText($oGroup->group_name);
            $groupKey = strstr($groupName," ",true);
            $current = isset($currentGroups[$groupKey]) ? $currentGroups[$groupKey] : "1";
            $currentEm = ($current!=="" && $current!=="1") ? $this->_getHtmlExpression($current,$iSurveyId,array(),true) : "";
            $aSurveySettings[$groupName." ({$oGroup->gid})"]=array(
                array(
                    'id'=>'group_'.$oGroup->gid,
                    'name'=>'group['.$groupKey.']',
                    'label' => 'Groupe: '.viewHelper::flatEllipsizeText($oGroup->group_name,1,50),
                    'current'=>isset($currentGroups[$groupKey]) ? $currentGroups[$groupKey] : "",
                    'help'=>($currentEm!=="") ? sprintf($this->_translate("Expression : %s"),$currentEm) : "",
                    'type' => 'group',
                ),
            );
            /* Get questions */
            $oQuestions= Question::model()->findAll(array(
                'condition'=>"gid=:gid and language=:language and parent_qid=0",
                'order'=>'question_order ASC',
                'params'=>array(":gid"=>$oGroup->gid,":language"=>$oSurvey->language),
            ));
            foreach($oQuestions as $oQuestion) {
                $current = isset($currentQuestions[$oQuestion->title]) ? $currentQuestions[$oQuestion->title] : "";
                $currentEm = ($current!=="" && $current!=="1") ? $this->_getHtmlExpression($current,$iSurveyId,array(),true) : "";
                $linkToQuestion = CHtml::link($oQuestion->title,
                    array('admin/questions',
                        'sa'=>'view',
                        'surveyid'=>$oQuestion->sid,
                        'gid'=>$oQuestion->gid,
                        'qid'=>$oQuestion->qid
                    )
                );
                $aSurveySettings[$groupName." ({$oGroup->gid})"][]=array(
                    'id'=>'question_'.$oQuestion->title,
                    'name'=>'question['.$oQuestion->title.']',
                    'label' => "[<strong>".$linkToQuestion."</strong>] ".viewHelper::flatEllipsizeText($oQuestion->question,1,50),
                    'current'=>$current,
                    'help'=>($currentEm!=="") ? sprintf($this->_translate("Expression : %s"),$currentEm) : "",
                    'type' => 'question',
                );
                // Find subquestion (all scales)
                $oSubQuestions=Question::model()->findAll(array(
                    'condition'=>"gid=:gid and parent_qid=:qid and language=:language",
                    'order'=>'scale_id ASC,question_order ASC',
                    'params'=>array(":gid"=>$oGroup->gid,":qid"=>$oQuestion->qid,":language"=>$oSurvey->language),
                ));
                foreach($oSubQuestions as $oSubQuestion) {
                    $scale=intval($oSubQuestion->scale_id);
                    $current = isset($currentSubQuestions[$oQuestion->title][$scale][$oSubQuestion->title]) ? $currentSubQuestions[$oQuestion->title][$scale][$oSubQuestion->title] : "";
                    $currentEm = ($current!=="" && $current!=="1") ? $this->_getHtmlExpression($current,$iSurveyId,array(),true) : "";
                    $aSurveySettings[$groupName." ({$oGroup->gid})"][]=array(
                        'id'=>'subquestion_'.$oQuestion->title.'_'.$scale.'_'.$oSubQuestion->title,
                        'name'=>'subquestion['.$oQuestion->title.']['.$scale.']['.$oSubQuestion->title.']',
                        'label' => "[<strong>".$oSubQuestion->title."</strong>] ".viewHelper::flatEllipsizeText($oSubQuestion->question,1,50),
                        'current'=>$current,
                        'help'=>($currentEm!=="") ? sprintf($this->_translate("Expression : %s"),$currentEm) : "",
                        'type' => 'subquestion-s'.$scale,
                    );
                }
                // Find answers
                $oAnswers=Answer::model()->findAll(array(
                    'condition'=>"qid=:qid and language=:language",
                    'order'=>'scale_id ASC,sortorder ASC',
                    'params'=>array(":qid"=>$oQuestion->qid,":language"=>$oSurvey->language),
                ));
                foreach($oAnswers as $oAnswer) {
                    $current = isset($currentAnswers[$oQuestion->title][$oAnswer->scale_id][$oAnswer->code]) ? $currentAnswers[$oQuestion->title][$oAnswer->scale_id][$oAnswer->code] : "";
                    $currentEm = ($current!=="" && $current!=="1") ? $this->_getHtmlExpression($current,$iSurveyId,array(),true) : "";
                    $aSurveySettings[$groupName." ({$oGroup->gid})"][]=array(
                        'id'=>'answer_'.$oQuestion->title.'_'.$oAnswer->scale_id.'_'.$oAnswer->code,
                        'name'=>'answer['.$oQuestion->title.']['.$oAnswer->scale_id.']['.$oAnswer->code.']',
                        'label' => "[<strong>".$oAnswer->code."</strong>] ".viewHelper::flatEllipsizeText($oAnswer->answer,1,50),
                        'current'=>$current,
                        'help'=>($currentEm!=="") ? sprintf($this->_translate("Expression : %s"),$currentEm) : "",
                        'type' => 'answer',
                    );
                }
            }
        }
        return $aSurveySettings;
    }

    /**
     * Get the current textarea settings for a survey
     * @param int surveyId
     * @param be sure to update/reset current value (EM session)
     * @return array[]
     **/
    private function _getSurveyText($iSurveyId) {
        $aTextSettings = array();
        foreach(self::_getTextLabels() as $textSetting=>$label) {
            $current = $this->get($textSetting, 'Survey', $iSurveyId,"");
            if(is_array($current)) {
                $current = end($current);
            }
            $currentEm = ($current!=="") ? $this->_getHtmlExpression($current,$iSurveyId,array(),false) : "";
            /* Find if current EM have error …*/
            $haveError = strpos($currentEm,"em-expression em-haveerror");
            $aTextSettings[$textSetting]=array(
                'type' => 'text',
                'label'=> $label,
                'controlOptions'=>array(
                    'class' => $haveError ? 'has-error' : "",
                ),
                'labelOptions'=>array(
                    'class' => $haveError ? 'text-danger' : "",
                ),
                'current'=>$current,
            );
        }
        return $aTextSettings;
    }

    /**
     * Get the text current settings for a survey
     * @param int surveyId
     * @param be sure to update/reset current value (EM session)
     * @return array[]
     **/
    private function _getSurveyString($iSurveyId) {
        $aTextSettings = array();
        foreach(self::_getStringLabels() as $textSetting=>$label) {
            $current = $this->get($textSetting, 'Survey', $iSurveyId,"");
            if(is_array($current)) {
                $current = end($current);
            }
            $currentEm = ($current!=="") ? $this->_getHtmlExpression($current,$iSurveyId,array(),false) : "";
            /* Find if current EM have error …*/
            $haveError = strpos($currentEm,"em-expression em-haveerror");
            $aTextSettings[$textSetting]=array(
                'type' => 'string',
                'label'=> $label,
                'controlOptions'=>array(
                    'class' => $haveError ? 'has-error' : "",
                ),
                'labelOptions'=>array(
                    'class' => $haveError ? 'text-danger' : "",
                ),
                'current'=>$current,
            );
        }
        foreach(self::_getSettingsStringLabels() as $textSetting=>$label) {
            $current = $this->get($textSetting, 'Survey', $iSurveyId,"");
            if(is_array($current)) {
                $current = end($current);
            }
            $currentEm = ($current!=="") ? $this->_getHtmlExpression($current,$iSurveyId,array(),false) : "";
            /* Find if current EM have error …*/
            $haveError = strpos($currentEm,"em-expression em-haveerror");
            $aTextSettings[$textSetting]=array(
                'type' => 'string',
                'label'=> $label,
                'controlOptions'=>array(
                    'class' => $haveError ? 'has-error' : "",
                ),
                'labelOptions'=>array(
                    'class' => $haveError ? 'text-danger' : "",
                ),
                'current'=>$current,
            );
        }
        return $aTextSettings;
    }

    /**
     * Get the aray of text with labels
     * @return array[]
     **/
    private static function _getTextLabels() {
        return array(
            'description'=>gT("Survey description"),
            'welcometext'=>gT("Welcome message:"),
            'endtext'=>gT("End message:"),
            'email_invite'=>gT("Invitation email body:"),
            'email_remind'=>gT("Reminder email body:"),
            'email_register'=>gT("Registration email subject:"),
            'email_confirm'=>gT("Confirmation email body:"),
        );
    }
    /**
     * Get the aray of string with labels
     * @return array[]
     **/
    private static function _getStringLabels() {
        return array(
            'url'=>gT("End URL:"),
            'urldescription'=>gT("URL description:"),
        );
    }
    /**
     * Get the aray of string with labels
     * @return array[]
     **/
    private static function _getSettingsStringLabels() {
        return array(
            'admin'=>gT("Administrator:"),
            'adminemail'=>gT("Admin email:"),
        );
    }
    public function afterSurveyComplete() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $iSurveyId = $this->getEvent()->get('surveyId');
        $iResponseId = $this->getEvent()->get('responseId');
        $iBaseSurvey = $this->get('surveyBaseId', 'Survey', $iSurveyId);
        if(is_array($iBaseSurvey)) {
            $iBaseSurvey = end($iBaseSurvey);
        }
        if(!$iBaseSurvey) {
            return;
        }
        if(!Permission::model()->hasGlobalPermission('surveys','create')) {
            $messageToShown = Yii::app()->getConfig('surveyBySurveyGeneratorMessage');
            if(empty($messageToShown)) {
                $messageToShown = $this->gT("Vous n‘avez pas les droits suffisant pour créer un questionnaire.");
            }
            $messageClass = Yii::app()->getConfig('surveyBySurveyGeneratorWarningClass');
            if(empty($messageClass)) {
                $messageClass = "surveyBySurveyGenerator-message alert alert-warning";
            }
            $this->getEvent()->getContent($this)
            ->addContent('<div class="'.$messageClass.'">'.$messageToShown.'</div>');
            return;
        }
        $oBaseSurvey = Survey::model()->findByPk($iBaseSurvey);
        if(!$oBaseSurvey) {
            return;
        }
        $oEvent = new PluginEvent('beforeCopySurveyGenerator');
        $this->log('beforeCopySurveyGenerator');
        $oEvent->set('generatorId', $iSurveyId);
        $oEvent->set('sourceId', $baseSurveyId);
        $oEvent->set('surveyBySurveyGeneratorMessage', "");
        App()->getPluginManager()->dispatchEvent($oEvent);
        $messageToShown = $oEvent->get('surveyBySurveyGeneratorMessage');
        if(!empty($messageToShown)) {
            $messageClass = $oEvent->get('surveyBySurveyGeneratorAlertType','warning');
            \renderMessage\messageHelper::renderAlert($messageToShown,$messageClass);
            //~ $messageToShown = '<div class="'.$messageClass.'">'.$messageToShown.'</div>';
            Yii::app()->end();
        }
        $oBaseSurveyLanguage = SurveyLanguageSetting::model()->findByPk(array('surveyls_survey_id'=>$iBaseSurvey,'surveyls_language'=>$oBaseSurvey->language));
        $aSurveySettings = $this->_getSurveySettings($iSurveyId,false);
        $aReData['thissurvey']=getSurveyInfo($iSurveyId);
        if($iBaseSurvey && !empty($aSurveySettings)) {
            $aCurrents = array();
            foreach($aSurveySettings as $group=>$aGroups) {
                foreach($aGroups as $aSetting) {
                    $current = trim($aSetting['current']);
                    $value = 1;
                    if($current !== "") {
                        $value = templatereplace("{".$current."}", array(),$aReData,__CLASS__,false,null,array(),true);
                    }
                    $aCurrents[] = array(
                        'id' => $aSetting['id'],
                        'value' => (bool)$value,
                    );
                }
            }
            /* Backup new language (else, after : seems broken (when reloading)*/
            $aSurveyText=$this->_getTextLabels();
            $aNewSurveyText = array();
            foreach($aSurveyText as $textSetting=>$label) {
                $currentTextValue = $this->get($textSetting, 'Survey', $iSurveyId,"");
                if(is_array($currentTextValue)) {
                    $currentTextValue = end($currentTextValue);
                }
                if(trim($currentTextValue)) {
                    $currentTextValue = LimeExpressionManager::ProcessString($currentTextValue,null,array(),1,1,false,false,true); // Get static value
                    //$currentTextValue=$this->_getHtmlExpression($currentTextValue,$iSurveyId,array(),false,false,true);
                    if(trim($currentTextValue)) {
                        if($oBaseSurvey->htmlemail=='Y' || substr($textSetting,0,6 ) !== "email_") {
                            // Replace bew line by <br>
                            $currentTextValue = nl2br(trim($currentTextValue));
                        }
                        $aNewSurveyText[$textSetting] = $currentTextValue;
                        
                    }
                }
            }
            $aSurveyString = $this->_getStringLabels();
            foreach($aSurveyString as $textSetting=>$label) {
                $currentTextValue = $this->get($textSetting, 'Survey', $iSurveyId,"");
                if(is_array($currentTextValue)) {
                    $currentTextValue = end($currentTextValue);
                }
                if(trim($currentTextValue)) {
                    $currentTextValue = LimeExpressionManager::ProcessString($currentTextValue,null,array(),1,1,false,false,true); // Get static value
                    //$currentTextValue=$this->_getHtmlExpression($currentTextValue,$iSurveyId,array(),false,false,true);
                    if(trim($currentTextValue)) {
                        $aNewSurveyText[$textSetting] = $currentTextValue;
                    }
                }
            }
            $aNewSurveySettings = array();
            $aSurveySeetingsString = $this->_getSettingsStringLabels();
            foreach($aSurveySeetingsString as $textSetting=>$label) {
                $currentTextValue = $this->get($textSetting, 'Survey', $iSurveyId,"");
                if(is_array($currentTextValue)) {
                    $currentTextValue = end($currentTextValue);
                }
                if(trim($currentTextValue)) {
                    $currentTextValue = LimeExpressionManager::ProcessString($currentTextValue,null,array(),1,1,false,false,true); // Get static value
                    //$currentTextValue=$this->_getHtmlExpression($currentTextValue,$iSurveyId,array(),false,false,true);
                    if(trim($currentTextValue)) {
                        $aNewSurveySettings[$textSetting] = $currentTextValue;
                    }
                }
            }
            /* Copy the survey */
            $aExcludes = array();
            $sNewSurveyName = str_replace("[BASE] ","",$oBaseSurveyLanguage->surveyls_title)." - ".date("c");
            $aExcludes['dates'] = true;
            $btranslinksfields=true;
            Yii::app()->loadHelper('export');
            $copysurveydata = surveyGetXMLData($iBaseSurvey, $aExcludes);
            if($copysurveydata) {
                /* Update string in $copysurveydata ? */
                /* [Q102] */
                $aReplacement=$this->_getReplacement($iSurveyId);
                $copysurveydata=str_replace(array_keys($aReplacement),array_values($aReplacement),$copysurveydata);
                Yii::app()->loadHelper('admin/import');
                $aImportResults = XMLImportSurvey('', $copysurveydata, $sNewSurveyName,NULL,$btranslinksfields);
            }
            if(!empty($aImportResults['newsid'])) {
                $oNewSurvey = Survey::model()->findByPk($aImportResults['newsid']);
                $oGroups= QuestionGroup::model()->findAll(array(
                    'condition'=>"sid=:sid and language=:language",
                    'order'=>'group_order ASC',
                    'params'=>array(":sid"=>$oNewSurvey->sid,":language"=>$oNewSurvey->language),
                ));
                /* Remove uneeded part */
                $groupCount = 0;
                foreach($aCurrents as $aCurrent) {
                    $aCurrentId = explode("_",$aCurrent['id']);
                    $type = $aCurrentId['0'];
                    switch($type) {
                        case 'group':
                            if(!$aCurrent['value']) {
                                $oThisGroup = $oGroups[$groupCount];
                                QuestionGroup::deleteWithDependency($oThisGroup->gid,$oNewSurvey->sid);
                            }
                            $groupCount++;
                            break;
                        case 'question':
                            $questionId = null;
                            $title = $aCurrentId['1'];
                            $oQuestion = Question::model()->find(array(
                                'condition'=>"title=:title and sid=:sid and language=:language and parent_qid=0",
                                'params'=>array(":title"=>$title,":sid"=>$oNewSurvey->sid,":language"=>$oNewSurvey->language)
                            ));
                            if($oQuestion) {
                                $questionId = $oQuestion->qid;
                                if(!$aCurrent['value']) {
                                    Question::deleteAllById($questionId);
                                    $questionId = null;
                                }
                            }
                            break;
                        case 'subquestion':
                            if($questionId) {
                                $scale = $aCurrentId['2'];
                                $title = $aCurrentId['3'];
                                if(!$aCurrent['value']) {
                                    $oQuestion = Question::model()->find(array(
                                        'condition'=>"title=:title and sid=:sid and language=:language and scale_id=:scale and parent_qid=:parent",
                                        'params'=>array(":title"=>$title,":sid"=>$oNewSurvey->sid,":language"=>$oNewSurvey->language,":scale"=>$scale,":parent"=>$questionId)
                                    ));
                                    Question::model()->deleteAllById($oQuestion->qid);
                                    // Find if we still need question
                                    $countSubQuestion = Question::model()->count(array(
                                        'condition'=>"language=:language and parent_qid=:parent",
                                        'params'=>array(":language"=>$oNewSurvey->language,":parent"=>$questionId)
                                    ));
                                    if(intval($countSubQuestion) === 0) {
                                        Question::deleteAllById($questionId);
                                        $questionId = null;
                                    }
                                }
                            }
                            break;
                        case 'answer':
                            if($questionId) {
                                $scale = $aCurrentId['2'];
                                $code = $aCurrentId['3'];
                                if(!$aCurrent['value']) {
                                    Answer::model()->deleteAll(array(
                                        'condition'=>"code=:code and scale_id=:scale and qid=:qid",
                                        'params'=>array(":code"=>$code,":scale"=>$scale,":qid"=>$questionId)
                                    ));
                                    // Find if we still need question
                                    $countAnswers = Answer::model()->count(array(
                                        'condition'=>"language=:language and qid=:qid",
                                        'params'=>array(":language"=>$oNewSurvey->language,":qid"=>$questionId)
                                    ));
                                    if(intval($countAnswers) === 0) {
                                        Question::deleteAllById($questionId);
                                        $questionId = null;
                                    }
                                }
                            }
                            break;
                        default:
                            // We must not get here : throw error ?
                    }
                }
                /* Update global parameters */
                $oNewSurvey->expires = null;
                $oNewSurvey->startdate = null;
                $iUserId=Yii::app()->session['loginID'];
                $oCurrentUser = User::model()->findByPk($iUserId);
                $oNewSurvey->owner_id = $iUserId;
                $oNewSurvey->adminemail = $oCurrentUser->email;
                $oNewSurvey->admin = $oCurrentUser->full_name;
                foreach($aNewSurveySettings as $surveySetting=>$newSurveySetting) {
                    $oNewSurvey->$surveySetting = $newSurveySetting;
                }
                $oNewSurvey->save();
                //~ $this->set('surveyBaseId','survey',$oNewSurvey->id,null);
                /* Update the string */
                $oNewSurveyLanguage = SurveyLanguageSetting::model()->findByPk(array('surveyls_survey_id'=>$aImportResults['newsid'],'surveyls_language'=>$oBaseSurvey->language));
                foreach($aNewSurveyText as $textSetting=>$newSurveyText) {
                    $attribute = 'surveyls_'.$textSetting;
                    // Html entities ?
                    $oNewSurveyLanguage->$attribute = $newSurveyText;
                }
                $oNewSurveyLanguage->save();
                PluginSetting::model()->deleteAll("plugin_id = :plugin_id and model_id = :model_id", array(':plugin_id'=>$this->id,':model_id'=>$aImportResults['newsid']));
                $oEvent = new PluginEvent('afterSurveyGeneratorCopy');
                $oEvent->set('surveyId', $aImportResults['newsid']);
                $oEvent->set('generatorId', $iSurveyId);
                $oEvent->set('sourceId', $iBaseSurvey);
                $oEvent->set('surveyBySurveyGeneratorMessage', "");
                App()->getPluginManager()->dispatchEvent($oEvent);
                $messageToShown = $oEvent->get('surveyBySurveyGeneratorMessage');
                if(empty($messageToShown)) {
                    $messageToShown = sprintf($this->gT("<a href='%s'>Administrer votre questionnaire</a>",'unescaped'),$this->api->createUrl('admin/survey/sa/view/surveyid/'.$aImportResults['newsid'], array()));
                }
                $messageClass = Yii::app()->getConfig('surveyBySurveyGeneratorWarningClass');
                if(empty($messageClass)) {
                    $messageClass = "surveyBySurveyGenerator-message alert alert-success";
                }
                $this->getEvent()->getContent($this)
                ->addContent('<div class="'.$messageClass.'">'.$messageToShown.'</div>');
                return;
            } else {
                $this->getEvent()->getContent($this)
                ->addContent('<div class="surveyBySurveyGenerator-message alert alert-danger">'.$this->gT('Une erreur durant la génération du questionnaire').'</div>');
                tracevar($aImportResults);
            }
        }
    }

    /**
    * Get the complete HTML from a string with Expression for admin
    * @param string $sExpression : the string to parse
    * @param array $aReplacement : optionnal array of replacemement
    * @param boolean $forceEm : force EM or not
    * @param boolean $expression : return EM with error to be shown to admin
    *
    * @author Denis Chenu
    * @version 1.0
    */
    private function _getHtmlExpression($sExpression,$iSurveyId=null,$aReplacement=array(),$forceEm=false,$expression=true,$iteration=1)
    {
        /* @var int current survey id (need to StartSurvey for LEM */
        static $currentSurveyId;
        $LEM = LimeExpressionManager::singleton();
        $aReData=array();
        //~ if($iSurveyId && $iSurveyId!=$currentSurveyId) {
            //~ $LEM::setSurveyId($iSurveyId);// replace QCODE
            //~ $currentSurveyId=$iSurveyId;
        //~ }
        $aReData['thissurvey']=getSurveyInfo($iSurveyId);
        if($forceEm) {
            $sExpression = "{".$sExpression."}";
        } else {
            $oFilter = new CHtmlPurifier();
            $sExpression =$oFilter->purify(\viewHelper::filterScript($sExpression));
        }
        if(!$expression) {
            //return LimeExpressionManager::ProcessString($sExpression,null,$aReplacement,false,$iteration,1,false,false,true);
            return templatereplace($sExpression, $aReplacement,$aReData,__CLASS__,false,null,array(),true);
        }

        templatereplace($sExpression, $aReplacement,$aReData,__CLASS__,false,null,array(),true);
        return $LEM::GetLastPrettyPrintExpression();


    }

    /**
     * Get the array of replacement for current survey
     * return array ["-(questionCode)-" => value]
     */
    private function _getReplacement($iSurveyId)
    {
        $aAllQuestionsCode=LimeExpressionManager::getLEMqcode2sgqa($iSurveyId);
        $aReplacements=array();
        foreach($aAllQuestionsCode as $code=>$value) {
            $replacement = LimeExpressionManager::ProcessString("{".$code.".shown}",null,array(),3,1,false,false,true); // Get static value
            if($replacement) {
                $aReplacements["-({$code})-"]=$replacement;
            }
        }
        return $aReplacements;
    }
    /**
     * get translation
     * @param string
     * @return string
     */
    private function _translate($string){
        return gT($string);
        //return Yii::t('',$string,array(),get_class($this));
    }
    
}
