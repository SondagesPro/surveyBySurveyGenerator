<div class="row">
    <div class="col-lg-12 content-right">
        <h3><?php echo $title ?></h3>
        <?php echo CHtml::beginForm();?>
        <?php foreach($aSettings as $legend=>$settings) {
          $this->widget('ext.SettingsWidget.SettingsWidget', array(
                //'id'=>'summary',
                'title'=>$legend,
                //'prefix' => $pluginClass, This break the label (id!=name)
                'form' => false,
                'formHtmlOptions'=>array(
                    'class'=>'form-core',
                ),
                'labelWidth'=>6,
                'controlWidth'=>6,
                'settings' => $settings,
            ));
        } ?>
        <?php foreach($aSurveySettings as $legend=>$aGroup) {
              echo CHtml::openTag('fieldset',array('class'=>'form-core group-field form-horizontal'));
              echo CHtml::tag('legend',array('class'=>'group-legend'),$legend);
                $closeFieldset=false;
                $closeFieldsetBis=false;
                $previousType=null;
                foreach($aGroup as $setting) {
                  if($setting['type']=="question") {
                    if($closeFieldsetBis) {
                       echo CHtml::closeTag('fieldset');
                    }
                    if($closeFieldset) {
                       echo CHtml::closeTag('fieldset');
                    }
                    echo CHtml::openTag('fieldset',array('class'=>'question-field'));
                    $closeFieldset=true;
                    $closeFieldsetBis=false;
                    $previousType=null;
                  } else {
                    if($previousType != $setting['type']) {
                      if($closeFieldsetBis) {
                         echo CHtml::closeTag('fieldset');
                      }
                      echo CHtml::openTag('fieldset',array('class'=>'sub-field '.$setting['type'].'field'));
                      $previousType=$setting['type'];
                      $closeFieldsetBis=true;
                    }
                  }
                  echo CHtml::openTag('div',array('class'=>'form-group setting setting-string setting-'.$setting['type']));
                  //echo CHtml::tag('pre',array(),print_r($element,1));
                  echo CHtml::label($setting['label'],$setting['id'],array('class'=>'default control-label col-sm-6'));
                  echo CHtml::openTag('div',array('class'=>'default col-sm-6 controls'));
                  echo CHtml::textField($setting['name'], $setting['current'], array('id'=>$setting['id'],'class'=>'form-control'));
                  if($setting['help']) {
                    echo CHtml::tag('div',array('class'=>'help-block'),$setting['help']);
                  }
                  echo CHtml::closeTag("div");
                  echo CHtml::closeTag("div");
                }
                
                if($closeFieldsetBis) {
                   echo CHtml::closeTag('fieldset');
                }
                if($closeFieldset) {
                   echo CHtml::closeTag('fieldset');
                }
              echo CHtml::closeTag("fieldset");
        } ?>
        <div class='row'>
          <div class='col-md-offset-6 submit-buttons'>
            <?php
              echo CHtml::htmlButton('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'save','class'=>'btn btn-primary'));
              echo " ";
              echo CHtml::htmlButton('<i class="fa fa-check-circle-o " aria-hidden="true"></i> '.gT('Save and close'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'redirect','class'=>'btn btn-default'));
              echo " ";
              echo CHtml::link(gT('Close'),Yii::app()->createUrl('admin/survey',array('sa'=>'view','surveyid'=>$surveyId)),array('class'=>'btn btn-danger'));
            ?>
            <div class='hidden' style='display:none'>
              <div data-moveto='surveybarid' class='pull-right hidden-xs'>
              <?php
              echo CHtml::link('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),"#",array('class'=>'btn btn-primary','data-click-name'=>'save'.$pluginClass,'data-click-value'=>'save'));
              echo " ";
              echo CHtml::link('<i class="fa fa-check-circle-o" aria-hidden="true"></i> '.gT('Save and close'),"#",array('class'=>'btn btn-default','data-click-name'=>'save'.$pluginClass,'data-click-value'=>'redirect'));
              echo " ";
              echo CHtml::link(gT('Close'),Yii::app()->createUrl('admin/survey',array('sa'=>'view','surveyid'=>$surveyId)),array('class'=>'btn btn-danger'));
              ?>
              </div>
            </div>
          </div>
        </div>
        <?php echo CHtml::endForm();?>
    </div>
</div>
<?php
  App()->clientScript->registerScriptFile($assetUrl.'/settings.js',CClientScript::POS_END);
  App()->clientScript->registerCssFile($assetUrl.'/settings.css');

?>
